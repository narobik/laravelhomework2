<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('prices')->insert(['confid' => 32,'price' => 500,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 76,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 106,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 88,'price' => 300,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 116,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 135,'price' => 250,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 127,'price' => 4400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 50,'price' => 13400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 70,'price' => 700,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 31,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 96,'price' => 3300,'unit' => 'kg ​​
']);
DB::table('prices')->insert(['confid' => 116,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 22,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 138,'price' => 4400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 112,'price' => 2900,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 58,'price' => 3200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 98,'price' => 10400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 75,'price' => 2100,'unit' => 'bar
']);
DB::table('prices')->insert(['confid' => 24,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 62,'price' => 600,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 61,'price' => 8400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 105,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 20,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 123,'price' => 1800,'unit' => 'bar
']);
DB::table('prices')->insert(['confid' => 60,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 24,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 38,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 126,'price' => 2100,'unit' => 'bar
']);
DB::table('prices')->insert(['confid' => 64,'price' => 750,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 109,'price' => 300,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 66,'price' => 350,'unit' => '
']);
DB::table('prices')->insert(['confid' => 89,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 98,'price' => 15400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 24,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 76,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 131,'price' => 250,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 50,'price' => 9200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 55,'price' => 600,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 87,'price' => 3400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 4,'price' => 3500,'unit' => 'wreath
']);
DB::table('prices')->insert(['confid' => 8,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 100,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 129,'price' => 5300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 35,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 47,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 89,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 111,'price' => 3300,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 94,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 42,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 80,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 134,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 128,'price' => 4000,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 90,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 39,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 71,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 17,'price' => 3400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 68,'price' => 18400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 81,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 134,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 108,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 97,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 81,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 44,'price' => 3800,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 72,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 49,'price' => 250,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 48,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 14,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 107,'price' => 12200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 27,'price' => 15400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 106,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 74,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 40,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 133,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 77,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 22,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 119,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 120,'price' => 3400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 105,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 119,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 99,'price' => 4600,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 61,'price' => 12200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 93,'price' => 4200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 59,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 82,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 56,'price' => 600,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 23,'price' => 550,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 81,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 67,'price' => 500,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 68,'price' => 6400,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 38,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 139,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 30,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 95,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 101,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 65,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 10,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 59,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 119,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 82,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 3,'price' => 3300,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 104,'price' => 4200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 110,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 1,'price' => 300,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 25,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 40,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 36,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 124,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 16,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 29,'price' => 3500,'unit' => 'wreath
']);
DB::table('prices')->insert(['confid' => 116,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 71,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 2,'price' => 500,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 71,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 10,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 108,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 69,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 39,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 25,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 107,'price' => 8400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 5,'price' => 9000,'unit' => '12 slices
']);
DB::table('prices')->insert(['confid' => 106,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 114,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 26,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 82,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 28,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 42,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 35,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 74,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 19,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 25,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 125,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 95,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 34,'price' => 1700,'unit' => 'bar
']);
DB::table('prices')->insert(['confid' => 121,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 76,'price' => 16200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 13,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 60,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 33,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 132,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 117,'price' => 9900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 27,'price' => 10400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 18,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 124,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 122,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 59,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 124,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 134,'price' => 13200,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 45,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 63,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 6,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 28,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 37,'price' => 3900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 52,'price' => 5000,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 61,'price' => 4500,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 86,'price' => 600,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 6,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 37,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 11,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 108,'price' => 7400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 35,'price' => 9000,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 107,'price' => 4500,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 6,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 79,'price' => 4000,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 60,'price' => 4300,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 21,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 28,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 15,'price' => 5000,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 21,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 37,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 74,'price' => 11400,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 103,'price' => 650,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 43,'price' => 4200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 12,'price' => 3400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 27,'price' => 5400,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 7,'price' => 490,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 84,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 115,'price' => 3600,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 51,'price' => 4000,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 118,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 41,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 135,'price' => 400,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 73,'price' => 5400,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 10,'price' => 8200,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 98,'price' => 5400,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 113,'price' => 850,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 130,'price' => 350,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 39,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 136,'price' => 3400,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 83,'price' => 650,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 91,'price' => 800,'unit' => '200 g
']);
DB::table('prices')->insert(['confid' => 46,'price' => 5200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 102,'price' => 330,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 95,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 54,'price' => 580,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 57,'price' => 530,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 22,'price' => 4700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 92,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 68,'price' => 12400,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 42,'price' => 5700,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 40,'price' => 10900,'unit' => '16 slices
']);
DB::table('prices')->insert(['confid' => 9,'price' => 450,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 78,'price' => 4200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 85,'price' => 500,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 137,'price' => 600,'unit' => 'pce
']);
DB::table('prices')->insert(['confid' => 50,'price' => 4900,'unit' => '8 slices
']);
DB::table('prices')->insert(['confid' => 38,'price' => 12100,'unit' => '24 slices
']);
DB::table('prices')->insert(['confid' => 53,'price' => 4200,'unit' => 'kg
']);
DB::table('prices')->insert(['confid' => 89,'price' => 4700,'unit' => '8 slices
']);
    }
}
