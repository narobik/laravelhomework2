<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ConfectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('confections')->insert(['cname' => 'Süni','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenyealagút','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sajtos pogácsa','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Diós-mákos','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sajttorta (málnás)','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Citrom','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Eszterházy','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Rákóczi-túrós','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Meggyes kocka','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Legényfogó','type' => 'cake','prizewinning' => '-1
']);
DB::table('confections')->insert(['cname' => 'Alpesi karamell','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Kókuszcsók','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Habos mákos','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Szilvás','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Juhtúrós párna','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mákos guba','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Néró','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sacher','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Citrom','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ribizlihabos-almás réteges','type' => 'special cake','prizewinning' => '-1
']);
DB::table('confections')->insert(['cname' => 'Három kívánság','type' => 'cake','prizewinning' => '-1
']);
DB::table('confections')->insert(['cname' => 'Dobos','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Epres mascarpone','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokoládémousse','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Oroszkrém','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Medvetalp','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Trüffel','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tejszínes gyümölcsös (meggy)','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mákos-szilvalekváros','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ribizlihabos-﻿almá﻿s réteges tortaszelet','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Marcipános vágott','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Indiáner','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Meggyes','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mákos','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sós karamella','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Legényfogó','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Rigó Jancsi','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tejszínes gyümölcsös (erdei gyümölcs)','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ez+Az (csokoládé és gesztenye)','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Málnás mascarpone','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Dobos','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ferrero','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Vegyes házi pitefalatok','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ökörszem','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Danubius kocka','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sajtkrémmel töltött fánkocska','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Túrókrém gyümölccsel díszítve','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Almás','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mignon','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokoládémousse fényes csokoládéval','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Vágott sós (sós omlós)','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Nagyi sós','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Vegyes sós','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Somlói','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tiramisu','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Hegyvidék','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Szedres csokoládé','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Pogácsák mixeden','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Lúdláb','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sacher','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Eszterházy','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Zalavári gesztenye','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenyegolyó','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Pisztáciás-málnás mascarpone','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Habos mákos','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Franciakrémes','type' => 'pastry','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenye kocka','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Pisztáciás-málnás mascarpone','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Málnás kocka','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sajttorta (málnás)','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Túrókrém gyümölccsel','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokis kaland','type' => 'special cake','prizewinning' => '-1
']);
DB::table('confections')->insert(['cname' => 'Somlói','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Palermo','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Szilvalekváros','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ünnepi diótorta grillázzsal','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Oroszkrém','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mini zserbó','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Sajtos masni','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Zserbó','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tejszínes gyümölcsös (málna)','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Marcipános csokoládé','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokis kaland','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Marcipán tekercs','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Képviselőfánk','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Epres omlett','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mini linzer','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Linzerkarika','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Szedres csokoládé','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Narancsív','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenyepüré','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Palermo','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokis néró','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Flódni','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mézeskalács','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Olívás pogácsa','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Florentin','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tiramisu','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Zoli kedvence (vágott édes tea)','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Erdei gyümölcs kocka','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Rákóczi-túrós','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mézeskrémes','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Trüffel','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Szilvás papucs','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Zalavári gesztenye','type' => 'cake','prizewinning' => '-1
']);
DB::table('confections')->insert(['cname' => 'Danubius','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Alpesi karamell','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Puncs','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenye szív','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ez+Az (csokoládé és gesztenye)','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tökmagos félhold','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Burgonyás pogácsa','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Somlói galuska','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Puncs','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Lekváros vágott','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Oreo','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Vintage','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Rigó Jancsi','type' => 'creamy cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Feketeerdő','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Kókuszos vágott','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Feketeerdő','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Moscauer','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Diós','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Rákóczi-túrós','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Három kívánság','type' => 'special cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenyés-karamellás','type' => 'roll','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Gesztenyés szív','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ropi','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Paleolit étcsokoládé','type' => 'special cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Túrós','type' => 'pie','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Ischler','type' => 'mixed','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Lúdláb','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Csokoládémousse','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Dió','type' => 'cake','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Krémes','type' => 'pastry','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mini ischler','type' => 'sweet biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Paleolit étcsokoládé','type' => 'cake slice','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Tejfölös túrós hajtogatott','type' => 'salty biscuit','prizewinning' => '0
']);
DB::table('confections')->insert(['cname' => 'Mákos guba','type' => 'cake','prizewinning' => '0
']);

    }
}
