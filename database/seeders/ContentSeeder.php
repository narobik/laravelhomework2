<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('contents')->insert(['confid' => 26,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 37,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 83,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 91,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 137,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 60,'free' => 'Te
']);
DB::table('contents')->insert(['confid' => 129,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 122,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 90,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 26,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 94,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 46,'free' => 'É
']);
DB::table('contents')->insert(['confid' => 72,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 114,'free' => 'Te
']);
DB::table('contents')->insert(['confid' => 63,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 12,'free' => 'Te
']);
DB::table('contents')->insert(['confid' => 128,'free' => 'É
']);
DB::table('contents')->insert(['confid' => 51,'free' => 'É
']);
DB::table('contents')->insert(['confid' => 109,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 109,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 97,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 97,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 24,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 91,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 137,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 84,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 30,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 108,'free' => 'Te
']);
DB::table('contents')->insert(['confid' => 84,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 6,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 108,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 12,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 79,'free' => 'É
']);
DB::table('contents')->insert(['confid' => 72,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 118,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 60,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 52,'free' => 'É
']);
DB::table('contents')->insert(['confid' => 137,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 114,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 90,'free' => 'To
']);
DB::table('contents')->insert(['confid' => 20,'free' => 'HC
']);
DB::table('contents')->insert(['confid' => 63,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 129,'free' => 'G
']);
DB::table('contents')->insert(['confid' => 129,'free' => 'L
']);
DB::table('contents')->insert(['confid' => 15,'free' => 'É
']);

    }
}
