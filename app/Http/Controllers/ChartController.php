<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ChartController extends Controller
{
    public function read(Request $request)
    {
		$cakes = DB::table('confections')->select(DB::raw('count(*) as type_count, type'))->groupBy('type')->get();
        return view('adminlte', compact('cakes'));
    }
}
