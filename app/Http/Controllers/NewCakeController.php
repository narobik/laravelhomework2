<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Confection;
class NewCakeController extends Controller
{
    public function create(Request $request)
    {
        $msg=array('mes'=>"Wrong data!");
        $this->validate($request,[
            'cname'=>'required|min:2',
            'type'=>'required',
            ]);
        $tomb=array('cname' =>$request->cname,'type' =>$request->type,'prizewinning' =>$request->prizewinning);
        Confection::create($tomb);
        $msg=array('mes'=>"Creation is done.");
        return view('newcake', $msg);
    }
}
