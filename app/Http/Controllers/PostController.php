<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        return view("index", compact('posts'));
    }

    public function create()
    {
        return view('create-post');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                "title" => "required",
                "description" => "required"
            ]
        );
        $post = Post::create($request->all());
        if(!is_null($post)) 
            return back()->with("success", "Success! Post created");
        else 
            return back()->with("failed", "Alert! Post not created");
    }

    public function show(Post $post)
    {
        return view('show-post', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('edit-post', compact('post'));
    }

    public function update(Request $request, Post $post)
    {
        $request->validate([
            "title" => "required",
            "description" => "required",
        ]);
        $post = $post->update($request->all());
        if(!is_null($post))
            return back()->with("success", "Success! Post updated");
        else
            return back()->with("failed", "Alert! Post not updated");
    }

    public function destroy(Post $post)
    {
        $post = $post->delete();
        if(!is_null($post))
            return back()->with("success", "Success! Post deleted");
        else
            return back()->with("failed", "Alert! Post not deleted");
    }
}
