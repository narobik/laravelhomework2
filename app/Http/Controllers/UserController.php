<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->paginate(5);
        return view("index2", compact('users'));
    }

    public function create()
    {
        return view('create-user');
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        if(!is_null($user)) 
        {
            return back()->with("success", "Success! User created");
            $request->user()->fill([
                'password' => Hash::make($request->newPassword)
            ])->save();
        }
        else 
            return back()->with("failed", "Alert! User not created");
    }

    public function show(User $user)
    {
        return view('show-user', compact('user'));
    }

    public function edit(User $user)
    {
        return view('edit-user', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user = $user->update($request->all());
        if(!is_null($user))
            return back()->with("success", "Success! User updated");
        else
            return back()->with("failed", "Alert! User not updated");
    }

    public function destroy(User $user)
    {
        $user = $user->delete();
        if(!is_null($user))
            return back()->with("success", "Success! User deleted");
        else
            return back()->with("failed", "Alert! User not deleted");
    }
}
