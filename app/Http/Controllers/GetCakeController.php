<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class GetCakeController extends Controller
{
    public function read(Request $request)
    {
		$cakes = DB::table('confections')->where('type', "=" , $request['type'])->get();
        return view('getcake', compact('cakes'));
    }
}
