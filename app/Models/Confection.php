<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Confection extends Model
{
    use HasFactory;
    protected $fillable = ['cname','type','prizewinning']; 

    public function content()
    {
        return $this->belongsTo(Content::class);
    }

    public function price()
    {
        return $this->hasMany(Price::class);
    }
}
