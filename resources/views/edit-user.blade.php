@extends("master")
@section("title") Update User @endsection
@section("content")
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 text-right">
            <a href="{{route('manage.index')}}" class="btn btn-dark"> Back to Users </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
            <form action="{{route('manage.update', $user->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card shadow">
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">× </button>
                            {{Session::get('success')}}
                        </div>
                    @elseif(Session::has('failed'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">× </button>
                            {{Session::get('failed')}}
                        </div>
                    @endif
                    <div class="card-header">
                        <h4 class="card-title"> Update User </h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title"> Name </label>
                            <input type="text" name="name" class="form-control" id="title" value="@if(!empty($user)) {{$user->name}} @endif">
                            {!!$errors->first("name", "<span class='text-danger'>:message </span>")!!}
                        </div>
                        <div class="form-group">
                            <label for="description"> Email </label>
                            <input type="email" class="form-control" name="email" id="title" value="@if(!empty($user)) {{$user->email}} @endif">
                            {!!$errors->first("email", "<span class='text-danger'>:message </span>") !!}
                        </div>
                        <div class="form-group">
                            <label for="description"> Role </label>
                            <input type="text" class="form-control" name="role" id="title" value="@if(!empty($user)) {{$user->role}} @endif">
                            {!!$errors->first("role", "<span class='text-danger'>:message </span>") !!}
                        </div>
                        <div class="form-group">
                            <label for="description"> Password </label>
                            <input type="password" class="form-control" name="password" id="title" value="@if(!empty($user)) {{$user->password}} @endif">
                            {!!$errors->first("password", "<span class='text-danger'>:message </span>") !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-secondary"> Update </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection