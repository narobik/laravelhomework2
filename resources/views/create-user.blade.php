@extends("master")
@section("title") Create User @endsection
@section("content")
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 text-right">
            <a href="{{route('manage.index')}}" class="btn btn-dark"> Back to Users </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
                <form action="{{route('manage.store')}}" method="POST">
                @csrf
                <div class="card shadow">
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">× </button>
                            {{Session::get('success')}}
                        </div>
                    @elseif(Session::has('failed'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">× </button>
                            {{Session::get('failed')}}
                        </div>
                    @endif
                    <div class="card-header">
                        <h4 class="card-title"> Create User </h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title"> Name </label>
                            <input type="text" name="name" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="title"> Email </label>
                            <input type="email" name="email" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="title"> Role </label>
                            <input type="number" name="role" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="title"> Password </label>
                            <input type="password" name="password" class="form-control" id="title">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-secondary"> Save </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection