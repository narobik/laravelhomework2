<?php $header = "Posts CRUD";?>
<x-app-layout><div class="hero d-flex justify-content-center align-items-center">
<x-slot name="header">
        {{ $header }}
    </x-slot>
  <body>
      <div class="hatter container-fluid mt-5">
          @yield('content')
      </div>
  </body>
</html>
</x-app-layout>