<?php $header = "Add a new Cake!";?>
<x-app-layout><div class="hero d-flex justify-content-center align-items-center">
<x-slot name="header">
        {{ $header }}
    </x-slot>
    <div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">

    <form method="post" action="getcake">
      @csrf 
      <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Enter a Cake Type</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="type">
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @if (isset($cakes) > 0)
    <ul class="list-group my-5">
      @foreach ($cakes as $cake)
        <li class="list-group-item">{{$cake->cname}}</li>
      @endforeach
    </ul>
    @endif  
  </div>
</div>
</x-app-layout>