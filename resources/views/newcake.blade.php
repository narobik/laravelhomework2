<?php $header = "Add a new Cake!";?>
<x-app-layout><div class="hero d-flex justify-content-center align-items-center">
<x-slot name="header">
        {{ $header }}
    </x-slot>
  <div class="container text d-flex flex-column my-5">

    <form method="post" action="newcake">
      @csrf 
      <div class="mb-3">
        <label for="exampleInput" class="form-label">Enter the Cake name </label>
        <input type="text" class="form-control" id="exampleInput" name="cname">
        <label for="exampleInput3" class="form-label">Cake type?</label>
        <input type="text" class="form-control" id="exampleInput3" name="type" >
        <label for="exampleInput4" class="form-label">Does the cake won a prize? (0 no-1 yes)</label>
        <input type="number" class="form-control" id="exampleInput4" name="prizewinning" min=0 max=1>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form> 

    @if (isset($mes))
    <h1 class="my-5">Successful addition</h1>
    @endif 
  </div>
</div>
</x-app-layout>