@extends("master")
@section("title") Show User @endsection
@section("content")
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 text-right">
            <a href="{{route('manage.index')}}" class="btn btn-dark"> Back to Users </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
            <div class="card shadow">
                <div class="card-header">
                    <h4 class="card-title"> Show Users </h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title"> Name </label>
                        <input type="text" readonly name="name" class="form-control" id="title" value="@if(!empty($user)) {{$user->name}} @endif">
                    </div>
                    <div class="form-group">
                        <label for="title"> Email </label>
                        <input type="email" readonly name="email" class="form-control" id="title" value="@if(!empty($user)) {{$user->email}} @endif">
                    </div>
                    <div class="form-group">
                        <label for="title"> Role </label>
                        <input type="text" readonly name="number" class="form-control" id="title" value="@if(!empty($user)) {{$user->role}} @endif">
                    </div>
                    <div class="form-group">
                        <label for="title"> Password </label>
                        <input type="password" readonly name="password" class="form-control" id="title" value="@if(!empty($user)) {{$user->password}} @endif">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection