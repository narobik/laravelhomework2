@extends("master")
@section("title") User @endsection
@section("content")
<div class="row mb-4">
    <div class="col-xl-6">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('failed'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('failed')}}
            </div>
        @endif 
    </div>
    <div class="col-xl-6 text-right">
        <a href="{{route('manage.create')}}" class="btn btn-dark "> Add New </a>
    </div>
</div>
<table class="table table-striped">
    <thead>
        <th> Id </th>
        <th> Name </th>
        <th> Email </th>
        <th> Action </th>
    </thead>
    <tbody>
        @if(count($users) > 0)
            @foreach($users as $user)
                <tr>
                    <td> {{$user->id}} </td>
                    <td> {{$user->name}} </td>
                    <td> {{$user->email}} </td>
                    <td>
                        <form action="{{route('manage.destroy', $user->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('manage.show', $user->id)}}" class="btn btn-sm btn-secondary"> Show </a>
                            <a href="{{route('manage.edit', $user->id)}}" class="btn btn-sm btn-secondary"> Edit </a>
                            <button type="submit" class="btn btn-sm btn-secondary"> Delete </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
{!! $users->links() !!}
@endsection