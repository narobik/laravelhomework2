<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); })->name("welcome");

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

use App\Http\Controllers\PostController;
Route::get('exercise1', function () { return view('view1'); });
Route::get('introduction', function () { return view('introduction'); })->name("introduction");
Route::group(['middleware' => 'auth2'], function () {     
    Route::get('user', function () { return view('user'); })->name("user");
    Route::get('admin', function () { return view('admin'); })->name("admin");
    Route::get('posts/create',[PostController::class,'create'])->name('posts.create');
    Route::post('posts',[PostController::class,'store'])->name('posts.store');
    Route::get('posts/{post}',[PostController::class,'show'])->name('posts.show');
    Route::get('posts/{post}/edit',[PostController::class,'edit'])->name('posts.edit');
    Route::put('posts/{post}',[PostController::class,'update'])->name('posts.update');
    Route::delete('posts/{post}',[PostController::class,'destroy'])->name('posts.destroy');
});


Route::get("newcake",function () { return view('newcake'); })->name("newcake");
Route::post('newcake', 'App\Http\Controllers\NewCakeController@create');

Route::get("getcake",function () { return view('getcake'); })->name("getcake");
Route::post('getcake', 'App\Http\Controllers\GetCakeController@read');

Route::get('posts',[PostController::class,'index'])->name('posts.index');

use App\Http\Controllers\UserController;
Route::get('manage',[UserController::class,'index'])->name('manage.index');
Route::get('manage/create',[UserController::class,'create'])->name('manage.create');
Route::post('manage',[UserController::class,'store'])->name('manage.store');
Route::get('manage/{user}',[UserController::class,'show'])->name('manage.show');
Route::get('manage/{user}/edit',[UserController::class,'edit'])->name('manage.edit');
Route::put('manage/{user}',[UserController::class,'update'])->name('manage.update');
Route::delete('manage/{user}',[UserController::class,'destroy'])->name('manage.destroy'); 

//Route::get('adminlte', function () { return view('adminlte'); })->name("adminlte");


Route::get('adminlte', 'App\Http\Controllers\ChartController@read')->name("adminlte");